# !/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    BP网络的前向传递过程
'''

import tensorflow as tf

INPUT_NODE = 784        # 输入结点，由于mnist数据集为28*28的图片，展开为一维数组即为784
OUTPUT_NODE = 10        # 输出结点，对应 0~9 的数字
LAYER1_NODE = 500       # 隐层结点

def get_weight(shape, regularizer):
    # 正则化，防止过拟合
    w = tf.Variable(tf.truncated_normal(shape, stddev=0.1))
    if regularizer != None:
        # w平方和正则化，将正则化项加入losses集合
        tf.add_to_collection('losses', tf.contrib.layers.l2_regularizer(regularizer)(w))
    return w

def get_bias(shape):
    # 偏置设置为0
    b = tf.Variable(tf.zeros(shape))
    return b

def forward(x, regularizer):

    # 隐层节点权值计算
    w1 = get_weight([INPUT_NODE, LAYER1_NODE], regularizer)
    b1 = get_bias([LAYER1_NODE])
    # 激活函数选用relu函数，当x小于零时为零，x大于零时为x
    y1 = tf.nn.relu(tf.matmul(x, w1) + b1)

    # 输出层权值计算
    w2 = get_weight([LAYER1_NODE, OUTPUT_NODE], regularizer)
    b2 = get_bias([OUTPUT_NODE])
    y = tf.matmul(y1, w2) + b2
    return y
