# !/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    BP网络的反向传递过程，训练模型并储存
'''

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import mnist_forward
import os

BATCH_SIZE = 200                # 训练批次的大小
LEARNING_RATE_BASE = 0.5        # 初始学习率
LEARNING_RATE_DECAY = 0.998      # 学习率衰减率
REGULARIZER = 0.0002            # 正则化权重
STEPS = 40001                   # 迭代次数
MOVING_AVERAGE_DECAY = 0.99     # 滑动平均衰减率
MODEL_SAVE_PATH = './model'     # 存储的模型的路径
MODEL_NAME = 'mnist_model'      # 存储模型的前缀名

def backward(mnist):
    # 输入数据
    x = tf.placeholder(tf.float32, [None, mnist_forward.INPUT_NODE])
    # 教师信号
    y_ = tf.placeholder(tf.float32, [None, mnist_forward.OUTPUT_NODE])
    y = mnist_forward.forward(x, REGULARIZER)
    global_step = tf.Variable(0, trainable=False)

    # 误差评价采用均方误差，从losses集合中获取正则化项
    loss = tf.reduce_mean(tf.square(y_-y)) + tf.add_n(tf.get_collection('losses'))

    # 学习率衰减
    learning_rate = tf.train.exponential_decay(
        LEARNING_RATE_BASE,
        global_step,
        mnist.train.num_examples / BATCH_SIZE,
        LEARNING_RATE_DECAY,
        staircase=True)

    # 梯度下降法
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

    # 滑动平均，记录了每个参数一段时间内过往值的平均，效果类似惯性环节，减弱数据顺序的影响，增强模型泛化性
    ema = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)

    # 对每个参数进行滑动平均修正，将操作加入计算图
    ema_op = ema.apply(tf.trainable_variables())
    with tf.control_dependencies([train_step, ema_op]):
        train_op = tf.no_op(name='train')

    # 初始化saver对象，用于保存模型
    saver = tf.train.Saver()

    # 开始训练
    with tf.Session() as sess:
        init_op = tf.global_variables_initializer()
        sess.run(init_op)

        for i in range(STEPS):
            # 从训练集中随机抽取BATCH_SIZE大小的数据对网络进行训练
            xs, ys = mnist.train.next_batch(BATCH_SIZE)
            _, loss_value, step = sess.run([train_op, loss, global_step], feed_dict={x: xs, y_: ys})
            if i % 1000 == 0:
                print("经过%d次迭代后，目前训练批次误差为%g。" % (step, loss_value))
                saver.save(sess, os.path.join(MODEL_SAVE_PATH, MODEL_NAME), global_step=global_step)

def main():
    # 获取mnist数据集，当文件不存在时自动下载
    mnist = input_data.read_data_sets('./data/', one_hot=True)
    backward(mnist)

if __name__ == '__main__':
    main()
