﻿### 简介 ###

* 基于 Tensorflow 框架实现了 BP 网络的搭建和对 mnist 数据集的数字识别
* 运行 ./mnist/mnist_backward.py 执行对模型的训练，运行 ./mnist/mnist_backward.py 执行对模型的测试
* 通过命令行运行时请保证运行目录添加到了python环境变量
* 程序编写参考了 Tensorflow 的官方 Get Started 文档
* 更多具体内容请参看源码

### 运行环境 ###

* Python 3.6
* Tensorflow-GPU 1.8
* CUDA 9.0
* Windows 10 Pro 1709
* 以上软件的相关的 dependencies
* NVIDIA GTX850M
* Intel Core i7-4710HQ

### 更多 ###

* 对本程序有任何疑问请联系我
* White_Xie.dl@foxmail.com
